package com.nfl.test;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class TestIntent {

	private Context context;

	public TestIntent(Context context) {
		super();
		// TODO Auto-generated constructor stub
		this.context = context;
		Intent intent = new Intent();
		/*
		 * 直接拨打电话
		 */
		// intent.setAction(Intent.ACTION_CALL);
		/*
		 * 填写好电话号码后，让用户选择拨打
		 */
		// intent.setAction(Intent.ACTION_DIAL);
		// intent.setData(Uri.parse("tel:13718719430"));
		/*
		 * 目前之发现“应用宝”可以以google market的格式搜索应用，搜狗输入法将pname作为搜索内容进行搜索 ， 但是搜索易失败
		 */
		// intent.setData(Uri.parse("market://search?q=pname:com.icbc")) ;
		/*
		 * latitude,longitude:纬经数据格式，在地图上显示经纬度指定的位置。目前之测试了百度地图可以定位经纬度
		 */
		// intent.setData(Uri.parse("geo:40,116"));
		context.startActivity(intent);
	}

}
