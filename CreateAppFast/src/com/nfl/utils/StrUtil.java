package com.nfl.utils;

import java.util.UUID;

public class StrUtil {
	/**
	 * UUID的生成 String 2015年11月16日
	 * 
	 * @return
	 */
	public static String getUUID() {
		String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
		return uuid;
	}
}
