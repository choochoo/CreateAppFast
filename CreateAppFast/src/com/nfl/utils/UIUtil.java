package com.nfl.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nfl.activity.R;
import com.nfl.views.WDialog;
import com.nfl.views.WToast;
import com.nfl.views.WzhDialog;

public class UIUtil {
	private static WToast toastUtil = null;
	private static WDialog dataLoadingDialog = null;

	public Context context = null;

	public UIUtil() {
	}

	public UIUtil(Context context) {
		this.context = context;
	}

	/**
	 * 判断用户是否注册
	 * 
	 * @param context
	 * @return
	 */
	// public static boolean isRegister(Context context){
	// SharedPreferences sp =
	// context.getSharedPreferences(WR.NSR_INFO,Activity.MODE_PRIVATE);
	// Set<String> set = sp.getStringSet(WR.W_NSRSBH_SET, null);
	//
	// if(null!=set && set.size()>0){
	// return true;
	// }else{
	// return false;
	// }
	// }

	// 未注册、网络未连接 提示
	public static void raDialog(Context context, String info) {
		if (null != context) {
			WzhDialog raDialog = new WzhDialog(context,
					R.layout.register_alert_dialog, R.style.dialogStyle);
			TextView tv = (TextView) raDialog
					.findViewById(R.id.tvAlertDialogMsg);
			tv.setText(info);
			raDialog.show();
		}
	}

	// 显示 "数据加载中" 提示对话框
	public static void showLoadingDialog(Context context) {
		if(isLoadingDialogShowing()){
			dataLoadingDialog.dismiss();
		}
		if (null != context) {
			dataLoadingDialog = new WDialog(context, R.layout.custom_dialog,
					R.style.dialogStyle);
			dataLoadingDialog.show();
		}
	}

	// 显示 "infomation" 提示对话框
	public static void showLoadingDialog(Context context, String infomation) {
		if(isLoadingDialogShowing()){
			dataLoadingDialog.dismiss();
		}
		if (null != context) {
			dataLoadingDialog = new WDialog(context, R.layout.custom_dialog,
					R.style.dialogStyle);
			((TextView) dataLoadingDialog.findViewById(R.id.tvDialogMsg))
					.setText(infomation);
			dataLoadingDialog.show();
		}
	}

	private static boolean isLoadingDialogShowing() {
		return null == dataLoadingDialog ? false : dataLoadingDialog
				.isShowing();
	}

	// 关闭 "数据加载中" 提示对话框
	public static void closeLoadingDialog() {
		if (null != dataLoadingDialog) {
			dataLoadingDialog.dismiss();
		}
	}

	// 短时Toast
	public static void shortToast(Context context, String info) {
		if (null != context) {
			toastUtil = WToast.makeText(context, info, Toast.LENGTH_SHORT);
			toastUtil.show();
		}
	}

	// 长时Toast
	public static void longToast(Context context, String info) {
		if (null != context) {
			toastUtil = WToast.makeText(context, info, Toast.LENGTH_LONG);
			toastUtil.show();
		}
	}

	// 校验用户输入Toast
	public static void validateToast(Context context, View view, String info) {
		if (null != context) {
			shortToast(context, info);
			view.requestFocus();
		}
	}

	// 校验用户输入Toast ,带有返回值
	public static boolean validateToastReturn(Context context, View view,
			String info) {
		if (null != context) {
			shortToast(context, info);
			view.requestFocus();
		}

		return false;
	}

	/*
	 * 关闭当前的Toast
	 */
	public static void cloaseToast() {
		if (toastUtil != null) {
			toastUtil.cancelCurrentAlert();
		}
	}

	/*
	 * 解决ScrollView 与ListView共存显示不完全的问题
	 */
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		int len = listAdapter.getCount();
		for (int i = 0; i < len; i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();

		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

		listView.setLayoutParams(params);
	}
}