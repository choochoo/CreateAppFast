package com.nfl.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.nfl.interfaces.GetImageFromServerCallBack;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

public class ImageUtils {

	/**
	 * 从网络上获得图片并设置在imageView上
	 */
	public static void setLogoImage(ImageView imageView, String url) {
		ImageLoader.getInstance().displayImage(url, imageView,
				new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						UIUtil.closeLoadingDialog();
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						// TODO Auto-generated method stub
						UIUtil.closeLoadingDialog();
					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						UIUtil.closeLoadingDialog();
					}
				});
	}

	/**
	 * 从网络上获得图片并设置在imageView上
	 */
	public static void displayImage(ImageView imageView, String url,
			final GetImageFromServerCallBack callBack) {

		ImageLoader.getInstance().displayImage(url, imageView,
				new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onLoadingFailed(String imageUri, View view,
							FailReason failReason) {
						// TODO Auto-generated method stub
						UIUtil.closeLoadingDialog();
					}

					@Override
					public void onLoadingComplete(String imageUri, View view,
							Bitmap loadedImage) {
						// TODO Auto-generated method stub
						callBack.updateView();
					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {
						// TODO Auto-generated method stub
						UIUtil.closeLoadingDialog();
					}
				});
	}
}
