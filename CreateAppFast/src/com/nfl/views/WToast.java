package com.nfl.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.nfl.activity.R;

@SuppressLint("HandlerLeak")
public class WToast {
	public static final int LENGTH_SHORT = 0;
    public static final int LENGTH_LONG = 1;
    private static View toastView;
    private WindowManager mWindowManager;
    private static int mDuration;
    private final int WHAT = 100;
    private static View oldView;
    private static Toast toast;
    private static CharSequence oldText;
    private static CharSequence currentText;
    private static WToast instance = null;
    private static TextView textView;
    
    @SuppressLint("ShowToast")
	private WToast(Context context) {
        mWindowManager = (WindowManager) context.getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);
        toastView = LayoutInflater.from(context).inflate(R.layout.toast_view,
                null);
        textView = (TextView) toastView.findViewById(R.id.toast_text);
        toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
    }
   
    private static WToast getInstance(Context context) {
        if (instance == null) {
            synchronized (WToast.class) {
                if (instance == null)
                    instance = new WToast(context);
            }
        }
        return instance;
    }
   
    public static WToast makeText(Context context, CharSequence text,
            int duration) {
        WToast util = getInstance(context);
        mDuration = duration;
        toast.setText(text);                
        currentText = text;
        textView.setText(text);
        return util;
    }
  
    public static WToast makeText(Context context, int resId, int duration){
        WToast util = getInstance(context);
        mDuration = duration;
        toast.setText(resId); 
        currentText = context.getResources().getString(resId);
        textView.setText(context.getResources().getString(resId));
        return util;
    }
    
    
    public void show() {
        long time = 0;
        switch (mDuration) {
        case LENGTH_SHORT:
            time = 2000;
            break;
        case LENGTH_LONG:
            time = 3500;
            break;
        default:
            time = 2000;
            break;
        }
        if (currentText.equals(oldText) && oldView.getParent() != null) {
            toastHandler.removeMessages(WHAT);
            toastView = oldView;
            oldText = currentText;
            toastHandler.sendEmptyMessageDelayed(WHAT, time);
            return;
        }
        cancelOldAlert();
        toastHandler.removeMessages(WHAT);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.format = PixelFormat.TRANSLUCENT;
        params.windowAnimations = android.R.style.Animation_Toast;
        params.type = WindowManager.LayoutParams.TYPE_TOAST;
        params.setTitle("Toast");
        params.gravity = toast.getGravity();
        params.y = toast.getYOffset();
        if (toastView.getParent() == null) {
            mWindowManager.addView(toastView, params);
        }
        oldView = toastView;
        oldText = currentText;
        toastHandler.sendEmptyMessageDelayed(WHAT, time);
    }
    
    

	@SuppressLint("HandlerLeak")
	private Handler toastHandler = new Handler() {
		@Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            cancelOldAlert();
            int id = msg.what;
            if (WHAT == id) {
                cancelCurrentAlert();
            }
        }
    };
    
    private void cancelOldAlert() {
    	//为了解决Pad问题
//        if (oldView != null ) {
//        	try{
//        		mWindowManager.removeView(oldView);
//        	}catch(Exception e){
//        		
//        	}
//            
//        }
    	
    	//如果只是手机应用，则要使用下面的代码，如果使用以上代码会报警告
    	if (oldView != null && oldView.getParent() != null) {
            mWindowManager.removeView(oldView);
        }
    }
    
    public void cancelCurrentAlert() {
    	//为了解决Pad问题
//    	if (toastView != null) {
//            try {
//                mWindowManager.removeView(toastView);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else if (oldView != null) {
//            try {
//                mWindowManager.removeView(oldView);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
    	
    	//如果只是手机应用，则要使用下面的代码，如果使用以上代码会报警告
    	if (toastView != null && toastView.getParent() != null) {
            mWindowManager.removeView(toastView);
        }
    }
}