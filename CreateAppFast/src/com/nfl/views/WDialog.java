package com.nfl.views;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

/*
 * 自定义Dialog
 */
public class WDialog extends Dialog{
	//默认尺寸
	private static final int defaultWidth = 160;
	private static final int defaultHeight = 120;
	
	public WDialog(Context context, int layout, int style){
		this(context,defaultWidth,defaultHeight,layout,style);
	}
	
	public WDialog(Context context, int width, int height, int layout, int style) {
		 super(context,style);
		 
		 setContentView(layout);
		 
		 Window window = getWindow();
		 
		 WindowManager.LayoutParams params = window.getAttributes();
		 
		 float density = getDensity(context);
         params.width = (int) (width*density);
         params.height = (int) (height*density);
         params.gravity = Gravity.CENTER;
         
         window.setAttributes(params);
	}
	
	private float getDensity(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
       return dm.density;
    }
}
