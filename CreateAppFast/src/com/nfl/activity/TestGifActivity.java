package com.nfl.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ViewFlipper;

/**
 * 通过ViewFlipper实现gif图片
 * @author nfl
 * @date 2015年11月27日
 */
public class TestGifActivity extends Activity {
	/** Called when the activity is first created. */
	private final String TAG = "JpgTestActivity";

	private ViewFlipper vf;
	private ViewFlipper vfNum;
	private int[] imgs = { R.drawable.hot01, R.drawable.hot02 };
	private int[] imgsNum = { R.drawable.hot01, R.drawable.hot02,
			R.drawable.hot03 };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_testgif);

		vf = (ViewFlipper) findViewById(R.id.vf_test1);
		vfNum = (ViewFlipper) findViewById(R.id.vf_test2);

		for (int i = 0; i < imgs.length; i++) {
			ImageView iv = new ImageView(this);
			iv.setImageResource(imgs[i]);
			iv.setScaleType(ImageView.ScaleType.CENTER);
			vf.addView(iv, new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));
		}

		for (int i = 0; i < imgsNum.length; i++) {
			ImageView iv = new ImageView(this);
			iv.setImageResource(imgsNum[i]);
			iv.setScaleType(ImageView.ScaleType.CENTER);
			vfNum.addView(iv, new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));
		}
		vf.setAutoStart(true);
		vf.setFlipInterval(200);
		vf.startFlipping();
		vfNum.setAutoStart(true);
		vfNum.setFlipInterval(200);
		vfNum.startFlipping();
	}
}
