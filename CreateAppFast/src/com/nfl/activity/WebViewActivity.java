package com.nfl.activity;

import com.nfl.utils.WebViewHelper;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;
import android.webkit.WebView;

public class WebViewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
						| WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		WebView mWebView = new WebView(this);
		WebViewHelper helper = new WebViewHelper(this, mWebView) ;
		helper.loadWithURL("http://122.81.104.150:9401/qdds_app/CoreServer?opr_id=4003");
		setContentView(mWebView);
	}
}
