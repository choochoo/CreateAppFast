package com.nfl.activity;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.example.qr_codescan.CaptureActivity;
import com.nfl.utils.FileUtil;
import com.nfl.utils.FlashlightUtil;
import com.nfl.utils.PhoneInfo;
import com.nfl.views.MyGifView;

public class MainActivity extends Activity {

	private MyGifView gif1, gif2;
	private ImageView img, rotate_test_iv, translate_test_iv, postGif;

	private Timer timer;
	private TimerTask timerTask;
	private int imgTag;
	private Button jniTest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		initView();
		sendMyBroadcast() ;
		// new TestIntent(this) ;// 由于该内容为空会出现异常，所以注释掉
		PhoneInfo.getScreenSize(this);
		/*
		 * 打开微信公众号
		 */
		Intent intent = new Intent(Intent.ACTION_VIEW); // 声明要打开另一个VIEW.

		String guanzhu_URL = "weixin://qdsbsqgsj"; // 这是你公共帐号的二维码的实际内容。可以用扫描软件扫一下就得到了。这是我的公共帐号地址。

		intent.setData(Uri.parse(guanzhu_URL)); // 设置要传递的内容。

		intent.setPackage("com.tencent.mm"); // 直接制定要发送到的程序的包名。也可以不制定。就会弹出程序选择器让你手动选木程序。

		intent.putExtra(Intent.EXTRA_SUBJECT, "Share");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// startActivity(intent); //当然要在Activity界面 调用了。

		timer = new Timer();
		timerTask = new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				postGif.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						imgTag = Integer.parseInt((String) postGif.getTag());
						switch (imgTag) {
						case 1:
							postGif.setTag("2");
							postGif.setImageResource(R.drawable.hot02);
							break;
						case 2:
							postGif.setTag("3");
							postGif.setImageResource(R.drawable.hot03);
							break;
						case 3:
							postGif.setTag("1");
							postGif.setImageResource(R.drawable.hot01);
							break;

						default:
							postGif.setTag("1");
							postGif.setImageResource(R.drawable.hot01);
						}

					}
				});
			}
		};
		timer.schedule(timerTask, 0, 200);

		// Intent testIntent = new Intent() ;
		// testIntent.setData(Uri.parse("https://www.baidu.com/s?wd=1")) ;
		// startActivity(testIntent);
	}

	@SuppressLint("NewApi")
	private void initView() {
		gif1 = (MyGifView) findViewById(R.id.gif1);
		gif1.setMovieResource(R.raw.gif);
		gif2 = (MyGifView) findViewById(R.id.gif2);
		gif2.setMovieResource(R.raw.gif);
		gif2.setPaused(true);
		img = (ImageView) findViewById(R.id.img);

		Matrix matrix = new Matrix();
		Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(
				R.drawable.half)).getBitmap();
		// 设置旋转角度
		// matrix.setRotate(90,20,20);
		// matrix.setTranslate(20, 20);
		// matrix.setSkew(0, 1, 20, 0);
		float[] src = new float[] { 0, 0, // 左上
				bitmap.getWidth(), 0,// 右上
				bitmap.getWidth(), bitmap.getHeight(),// 右下
				0, bitmap.getHeight() };// 左下
		float[] dst = new float[] { 0, 0, bitmap.getWidth(), 50,
				bitmap.getWidth(), bitmap.getHeight() - 50, 0,
				bitmap.getHeight() };
		matrix.setPolyToPoly(src, 0, dst, 0, src.length / 2);
		// 重新绘制Bitmap
		bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
				bitmap.getHeight(), matrix, true);
		img.setImageBitmap(bitmap);

		rotate_test_iv = (ImageView) findViewById(R.id.rotate_test_iv);
		Animation animationRotate = AnimationUtils.loadAnimation(this,
				R.anim.rotate_test);
		rotate_test_iv.startAnimation(animationRotate);

		translate_test_iv = (ImageView) findViewById(R.id.translate_test_iv);
		translate_test_iv.startAnimation(AnimationUtils.loadAnimation(this,
				R.anim.set_test));
		// translate_test_iv.startAnimation(AnimationUtils.loadAnimation(this ,
		// R.anim.translate_test));
		// translate_test_iv.startAnimation(AnimationUtils.loadAnimation(this ,
		// R.anim.scale_test));

		postGif = (ImageView) findViewById(R.id.postGif);

		jniTest = (Button) findViewById(R.id.jniTest);

	}

	public void open(View view) {
		File file = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath()
				+ File.separator
				+ "360"
				+ File.separator
				+ "test.ppt");
		FileUtil.openFile(file, getApplicationContext());
	}

	public void openGif(View view) {
		Intent intent = new Intent(this, TestGifActivity.class);
		startActivity(intent);
	}

	/**
	 * 扫描二维码
	 */
	public void openScanner(View view) {
		startActivity(new Intent(this, CaptureActivity.class));
	}

	/**
	 * 打开手电筒
	 */
	public void openFlashlight(View view) {
		FlashlightUtil.setSwitch();
	}

	static {
		System.loadLibrary("ffmpeg");
	}

	private void sendMyBroadcast() {
		// TODO Auto-generated method stub
		Intent intent = new Intent() ;
		intent.setAction(Intent.ACTION_DATE_CHANGED) ;
//		sendBroadcast(intent);
	}
}
